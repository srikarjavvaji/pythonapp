# Pre-Sales and Sales Pipeline Dashboard

## Requirements:  

> To install and run this application you will need

* Python 3.5+
* git (only to clone this repository)

## Installing

> The commands below set everything up to run the application

```
    python3 -m venv venv
    . venv/bin/activate
    (venv) pip install -r requirements.txt
```

> To run the application go to the src folder and  simply run

```
    (venv) python audiodash.py
```

## Deployment  using Docker Container 
 
> To get running docker file with details

```
sudo docker ps
```
> To replace container_id with running container_id
```
sudo docker rm -f dashdemo
```
> To build application
```
sudo docker build -t dashdemo .
```
> To run docker builded file to port 8060 and 8060
```
sudo docker run -d --restart unless-stopped -p 8060:8060 --name dash dashdemo
```


## Deploying Docker containers using Multibranch-pipeline project with Jenkinsfile(Declarative Pipeine)

1. Create a Multibranch-pipeline project in Jenkins and also install Docker based plugins in Jenkins
2. Now add Jenkinsfile to your Project
```
pipeline {
  agent any
    stages {
      stage('Docker Build') {
         steps {
          sh 'docker build -t dashdemo .'
              }
                       }
        stage('Docker container remove') {
         steps {
          sh 'docker rm -f dash'
               }  
                                         }             
        stage('Docker Run') {
         steps {
          sh 'docker run -d --restart unless-stopped -p 8060:8060 --name dash dashdemo'
               }
                           }
                           
  }
}
```
> Note: Agent name need to be changed as per the node requirement as follows 
```
agent { node { label 'labelName' } }
```
3. Now, Integrate Git repository URL/GitLab in your Jenkins Multibranch pipeline project
4. Execute below commands in Jenkins master and Nodes those required to deploy
```
sudo usermod -a -G docker jenkins
sudo chmod 777 /var/run/docker.sock
``` 
5. Either you can add auto build trigger in Gitlab or click on Build now button in multibranch pipeline project
6. Now check output in Gitlab or Jenkins/Blue Ocean.
